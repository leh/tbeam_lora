//links config tab
#include "config.h"
#include <WiFi.h>
#include <WiFiMulti.h>
#include <ArduinoJson.h>
#define BASEDELAY 60000
//500 for 1 fragment
StaticJsonDocument<1500> forcast;
StaticJsonDocument<200> actions;
WiFiMulti WiFiMulti;
#include <HTTPClient.h>
HTTPClient http;

void send_data() {
  //host to contact
  String host = "10.130.1.225:1880";
  //URL to get.
  String url = "/test_sensor/POP08";
  http.begin("http://" + host + url);
  int retCODE = http.GET();
  Serial.print("[HTTP] Return code =");
  Serial.println(retCODE);
  // All is working
  if (retCODE > 0) {

    if (retCODE == HTTP_CODE_OK) {
      String content = http.getString();
      Serial.println(content);
    }
    else {
      Serial.println("[HTTP]: Ooops.... Something's not right");
    }//end IF Code OK
  }
  // If an error
  else {
    Serial.print("[HTTP]GET failed, error:");
    Serial.println(retCODE);
  }//end if retcode=0;
  http.end();
}

void get_data() { //get data from OWM
  //host to contact
  String host = "api.openweathermap.org";
  //URL to get.
  //cnt= count of result
  //appid= API KEY
  String url = "/data/2.5/forecast?q=Willems,fr&cnt=1&mode=json&appid=" + String (myAPIKEY);
  http.begin("http://" + host + url);
  int retCODE = http.GET();
  Serial.print("[HTTP] Return code =");
  Serial.println(retCODE);
  // All is Working
  if (retCODE > 0) {

    if (retCODE == HTTP_CODE_OK) {
      String content = http.getString();
      //We have out Data ready
      Serial.println(content);
      //Let's try to parse it
      DeserializationError error = deserializeJson(actions, content);
      //Test if Parsing succeeds
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }//Endif parce Error

      //Parcing worked. grab values.
      float windSpeed = forcast["list"][0]["wind"]["speed"];
      int windDirection = forcast["list"][0]["wind"]["deg"];
      Serial.print("Wind Speed m/s=");
      Serial.println(windSpeed);
      Serial.print("Wind Direction (deg)=");
      Serial.println(windDirection);
    }
    else {
      Serial.println("[HTTP]: Ooops.... Something's not right");
    }//end IF Code OK
  }
  // If an error
  else {
    Serial.print("[HTTP]GET failed, error:");
    Serial.println(retCODE);
  }
}


void setup()
{
  //opening serial port at 115200
  Serial.begin(115200);
  delay(10);
  WiFiMulti.addAP(mySSID, myPASSWORD);
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void get_actions() {
  //host to contact
  String host = "10.130.1.225:1880";
  //URL to get.
  //cnt= count of result
  //appid= API KEY
  String url = "/getactions";

  http.begin("http://" + host + url);
  int retCODE = http.GET();

  Serial.print("[HTTP] Return code =");
  Serial.println(retCODE);
  // All is Working
  if (retCODE > 0) {

    if (retCODE == HTTP_CODE_OK) {
      String content = http.getString();
      //We have out Data ready
      Serial.println(content);
      //Let's try to parse it
      DeserializationError error = deserializeJson(actions, content);
      //Test if Parsing succeeds
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }//Endif parce Error
      //Parcing worked. grab values.

      if (actions["porte"] == 1) {
        Serial.println("OUVERTURE PORTE");
      }
      if (actions["porte"] == 0) {
        Serial.println("FERMETURE PORTE");
      }

    }
    else {
      Serial.println("[HTTP]: Ooops.... Something's not right");
    }//end IF Code OK
  }
  // If an error
  else {
    Serial.print("[HTTP]GET failed, error:");
    Serial.println(retCODE);
  }
}

void loop()//main loop
{
  //send_data();
  //get_data();
  get_actions();

  delay(50000);

}
